console.log("Hello World")


// SELECTING HTML ELEMENTS and ACCESSING ELEMENT PROPERTIES

/*
	HTML elements can be selected usin different "document" object methods/function
	It is useful for the properties and even attributes that can be manipulated using JS DOM
	- Using the JavaScript HTML DOM, the selected element contains a number of useful properties and even attributes that can be manipulated using JS DOM
	- For input fields, a "value" property is accessible to obtain information added by the user
	- Methods that select multiple elements returns an HTML collection which can be accessed similar to how array elements are accessed using their index

*/

	// const firstName = document.getElementById("first-name");
	// The querySelector method is a more flexible method for selecting different elements by their tag name, id and class names

const firstName = document.querySelector("#first-name");

const lastName = document.querySelector("#last-name");

const age = document.querySelector("#age");

const description = document.querySelector("#description");

const subscription = document.querySelector("#subscription");


console.log(firstName)

console.log(firstName.value)


// JAVASCRIPT EVENTS
/*
	- Responsible for making pages interactive
	- Using event listeners, JS can "listen" for the specific events that can be used

	SYNTAX:
	elementObject.addEventLister("event", function() {
		- statement or codes
	})
	- The "textContent" property defines the "text" displayed on HTML text elements like paragraphs and headers.
*/

const fullNameText = document.querySelector("#full-name-txt");
const ageText = document.querySelector("#age-txt")
const descriptionText = document.querySelector("#description-txt")

const subscriptionText = document.querySelector("#subscription-txt");

const priceText = document.querySelector("#price-txt")

// "textContent" property defines the "text" displayed in HTML elements line paragraphs and headers
// - The "innerHTML" property defines the HTML content of an HTML element which is useful for adding more HTML elements when needed

firstName.addEventListener("keyup", function() {
	console.log("FirstName")
	console.log(firstName.value)
	fullNameText.textContent = "Full Name: " + firstName.value + " " + lastName.value
})

lastName.addEventListener("keyup", function() {
	console.log("LastName")
	console.log(lastName.value)
	fullNameText.textContent = "Full Name: " + firstName.value + " " + lastName.value
})


// inner.HTML property defines HTML content of an HTML element useful for adding more HTML Elements when needed.
age.addEventListener("keyup", function () {
	console.log(age)
	// ageText.textContent = "Age: " + age.value
	ageText.innerHTML = "Age: Your age is <b> " + age.value + "</b>";
})

description.addEventListener("keyup", function () {
	console.log(description)
	descriptionText.textContent = "Description: " + description.value
})

subscription.addEventListener("change", function () {
	let subscriptionValue = subscription[subscription.selectedIndex].value

	subscriptionText.textContent = "Subscription Type" + subscriptionValue;

	if (subscriptionValue === "basic"){
		priceText.textContent = "Price: 500 PHP";

	} else {
		priceText.textContent = "Price: 1000 PHP"
	}
})
